from flask import Flask, render_template, request
import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
from io import BytesIO
import base64

app = Flask(__name__)

@app.route('/', methods = ['GET', 'POST'])
def index():
    if request.method == 'POST':
        file = request.files['csvfile']
        data = pd.read_csv(file, delimiter=",")
        img = BytesIO()
        df= pd.DataFrame(data)
        df.plot(x='x', y='y', style='o')
        plt.savefig(img, format='png')
        plt.close()
        img.seek(0)
        plot_url = base64.b64encode(img.getvalue()).decode('utf8')
        return render_template('plot.html', plot_url=plot_url)
    return render_template('dash.html')

if __name__ == '__main__':
    app.run(debug=True)